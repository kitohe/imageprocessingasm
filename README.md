# Aproksymacja półtonowa
|      Autor     | Rok akademicki | Semestr | Grupa | Sekcja | 
| ---------------| ---------------|---------|-------|--------| 
| Daniel Mądrala |    2018/2019   |    5    |   5   |    9   | 
## Opis:
Zadaniem aproksymacjii półtonowej jest przekształcenie obrazu o wiekszej liczbie odcieni szarości
lub barw w obraz monochromatyczny. Dodanie do algorytmu szumu wcześniej obliczonego na podstawie wzoru
*A·sinαx·sinβy* gdzie *A*, *α*, *β* to stale które są dobierane przez użytkownika, a zmienne *x* oraz *y*
to wspólrzedne kolejnych pikseli. Zastosowanie takiego podejscia pozwala na takie przybliżenie,
że punkty o podobnych wartościach znajdowały sie powyżej ustalonego progu przybliżenia a inne nie.
Dzięki temu uzyskujemy efekt łagodniejszego przejścia pomiedzy kolorem czarnym, a bialym.

### Efekty
| Original | Thresholding | Halftone-Approximation |
| -------- | ------------ | --------------------- |
| ![alt text](https://i.imgur.com/9ZxoErf.jpg "Original") | ![alt text](https://i.imgur.com/RzrXIcL.jpg "Thresholding") | ![alt text](https://i.imgur.com/zocr5sI.jpg"Twotone-Approximation") |

źródło obrazu: https://en.wiktionary.org/wiki/cat#/media/File:Cat03.jpg