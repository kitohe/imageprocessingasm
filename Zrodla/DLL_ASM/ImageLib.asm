.data
pixelarray			qword 0
transtionarray		qword 0
num_lines			dword 0
i_threshold			qword 0
i_leftover			dword 0
i_starting_point	dword 0
i_width				dword 0
i_height			dword 0
i_threads			dword 0
tmp					qword 0, 0, 0, 0
maskd				qword 18446742978492890880
maskaff				qword -1

test1				qword 0
test2				qword 0

counter				dword 0
.code

DllEntry PROC r:DWORD, g:DWORD, d:DWORD
	MOV EAX, 1
	RET
DllEntry ENDP

; calculating 2d arrays: https://stackoverflow.com/questions/44997734/how-to-access-2d-array-in-assembly-x86

process_img PROC pixel_array:QWORD, transition_array:QWORD, number_of_lines:DWORD, threshold:DWORD, leftover:DWORD, starting_point:DWORD, img_width:DWORD, img_height:DWORD, num_threads:DWORD

;	GET FUNCTION DATA TO LOCAL VARIABLES
;	------------------------------------
	mov qword ptr [pixelarray], rcx
	mov qword ptr [transtionarray], rdx
	mov dword ptr [num_lines], r8d
	mov qword ptr [i_threshold], r9	
	add rsp, 30h 
	pop rsi						; leftover
	pop rdi						; starting_point
	pop r8						; width
	pop r9						; height
	pop r10						; threads
	mov [i_leftover], esi
	mov [i_starting_point], edi
	mov [i_width], r8d
	mov [i_height], r9d	
	mov [i_threads], r10d

	;mov rax, [pixelarray]
	;mov rax, [transtionarray]
	;mov eax, [i_leftover]
	;mov eax, [i_starting_point]
	;mov eax, [i_width]
	;mov eax, [i_height]

;	SETUP
;	------------------------------------
	mov r8,	 [pixelarray]
	mov r9,  [transtionarray]
	mov r10, [i_threshold]

	xor rax, rax				; clear rax 
	mov rax, [i_threshold]		; threshold -> eax
	shld rax, rax, 20h			; bitshift left rax
	mov rbx, [i_threshold]		; threshold -> ebx
	or rax, rbx					; or rax, rbx
	mov [i_threshold], rax		; rax -> threshold

	movhps xmm14, [i_threshold]	; mov threshold to higher xmm14
	movlps xmm14, [i_threshold]	; mov threshold to lower xmm14
	vinsertf128 ymm14, ymm14, xmm14, 1h
	;vmovhps zmm14, ymm14
	mov r10, [transtionarray]	; move transitionarray to r10
	mov r11, [pixelarray]		; move pixelarray to r11

	xor rax, rax				; clear rax 
	xor rdi, rdi				; clear rdi
	mov eax, [i_starting_point]	; mov starting point to eax 
	add eax, [i_leftover]		; add leftover to eax
	add eax, [num_lines]		; add number of lines to eax
	mov ebx, eax
	mov eax, [i_width]
	mov ecx, [i_height]
	mul ecx
	cmp ebx, eax				; compare eax with height of image
	je finisher					; if not equal jump to 'standard' else jump to 'finisher'




;	ALGORITHIM
;	------------------------------------


standard:
	xor ecx, ecx
	mov r12d, [num_lines]
	mov eax, [i_starting_point]
	mov ebx, 4h
	mul ebx								; eax contains offset to base address (starting point offset)
	add rdi, rax

standard_loop:
	; ------------------------------------
	;               SSE VER
	; ------------------------------------
	;movaps xmm0, [r11+rdi]
	;movaps xmm1, [r10+rdi]	
	;paddd  xmm1, xmm14
	;pcmpgtd xmm0, xmm1
	;movaps [r11+rdi], xmm0


	; ------------------------------------
	;               AVX VER
	; ------------------------------------
	vmovups ymm0, [r11+rdi]
	vmovups ymm1, [r10+rdi]
	vpaddd  ymm1, ymm14, ymm1
	vpcmpgtd ymm0, ymm0, ymm1
	vmovups [r11+rdi], ymm0


	; ------------------------------------
	;             AVX512 VER
	; ------------------------------------
	;vmovups zmm0, [r11+rdi]
	;vmovups zmm1, [r10+rdi]
	;vpaddd zmm0, zmm14, zmm1
	;vpcmpgtd zmm0, zmm0, zmm1
	;vmovups [r11+rdi], zmm0


	add rdi, 20h  ; 10h for SSE - 20h for AVX - 30h for AVX512
	add ecx, 08h ; 04h for SSE - 08h for AVX - 0Ch for AVX512
	cmp ecx, r12d
	jne standard_loop
	jmp finish


finisher:
	sub eax, [i_starting_point]
	mov ecx, eax
	mov eax, [i_starting_point] 
	mov ebx, 4h
	mul ebx	
	mov edi, eax
	jmp algo_loop

algo_loop:

	;movaps xmm0, [r11+rdi]
	;movaps xmm1, [r10+rdi]
	;paddd  xmm1, xmm14
	;pcmpgtd xmm0, xmm1
	;movaps [r11+rdi], xmm0
	
	vmovups ymm0, [r11+rdi]
	vmovups ymm1, [r10+rdi]
	vpaddd  ymm1, ymm14, ymm1
	vpcmpgtd ymm0, ymm0, ymm1
	vmovups [r11+rdi], ymm0

	add rdi, 20h
	sub ecx, 08h
	test ecx, ecx
	jne algo_loop

finish:
	ret
process_img ENDP
END

	;


	;vmovups ymm0, ymmword ptr [r11+rdi]
	;vmovups ymm1, ymmword ptr [r10+rdi]
	;vpaddd ymm1, ymm14, ymm1
	;vcmpnltps ymm0, ymm1, ymm0
	;vmovups ymmword ptr [r11+rdi], ymm0


		;pextrd eax, xmm0, 0h
	;pextrd ebx, xmm0, 1h
	;pextrd r14d, xmm0, 2h
	;pextrd r15d, xmm0, 3h

	;xor eax, -1
	;xor ebx, -1
	;xor r14d, -1
	;xor r15d, -1

	;shl rax, 20h
	;shl r15, 20d
	;or rax, rbx
	;or r15, r14

	;mov [test1], rax
	;mov [test2], r15

	;movlps xmm0, [test1]
	;movhps xmm0, [test2]

	;pxor xmm0, xmm13
	;xorpd xmm0, xmm1
	;vpxor xmm0, xmm13,xmm0
	
	;vpxor xmm0, xmm13,xmm0
	;pandn xmm0, xmm0
	;cmpltps xmm0, xmm1
	;pxor xmm0, xmm13

		;psubd xmm1, xmm0
	;movaps xmm0, xmm1
	;ptest xmm0, xmm2
	;pcmpeqd xmm0, xmm2
	;pcmpgtq xmm0,

		;pxor xmm0, xmm0
	;pxor xmm1, xmm1

		;psignd xmm0, xmm13
	;pand xmm0, xmm0

	;cmpltps xmm0, xmm1
	;cmpltps xmm0, xmm1