#pragma once
extern "C"
{
	_declspec(dllexport) void process_img(int* array, int* t_array, unsigned number_of_lines, int threshold,
		unsigned leftover, unsigned starting_point, unsigned width, unsigned height, unsigned number_of_threads);
}