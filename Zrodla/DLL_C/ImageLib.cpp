#include <iostream>
#include "ImageLib.h"
#include <thread>
#include <fstream>
// extern "C"
// {
// 	_declspec(dllexport) void print()
// 	{
// 		std::cout << "TEST\n";
// 	}
// }

// int add(int a, int b)
// {
// 	std::cout << "DETECTED THREADS: " << detect_core_count() << std::endl;
// 	std::cout << "Calling from dll " << a + b << std::endl;
// 	return a + b;
// }

void process_img(int* array, int* t_array, unsigned number_of_lines, int threshold,
	unsigned leftover, unsigned starting_point, unsigned width, unsigned height, unsigned number_of_threads)
{
	//unsigned thre = (unsigned)threshold;
	if(starting_point + leftover + number_of_lines == height*width)
	{
		for(int i = starting_point; i < height * width; ++i)
		{		
			if(array[i] < threshold + t_array[i])
			{
				array[i] = 0x0;
			} 
			else
			{
				array[i] = 0xffffff;
			}
		}
	}
	else
	{
		for (int i = 0; i < number_of_lines; ++i)
		{				
			int j = i + starting_point;
			if (array[j] < threshold + t_array[j])
			{
				array[j] = 0x0;
			}
			else
			{
				array[j] = 0xffffff;
			}
		}
	}
}
