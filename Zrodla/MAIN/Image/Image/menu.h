#pragma once
#include <iostream>
#include "os_communication.h"
#include "algo_type.h"
#include "image_loader.h"

class menu
{
	os_communication comm;

	algo_info get_data();

	void select_version();

	void asm_mode();

	void cpp_mode();

	void both_mode();
public:
	void main_menu();
};
