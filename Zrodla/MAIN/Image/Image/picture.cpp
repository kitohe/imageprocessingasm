#include "picture.h"

picture::picture(bitmap_image image, algo_info info)
{
	this->width  = image.width();
	this->height = image.height();
	this->image = image;
	this->loader_ = new dll_loader();
	this->image_info = info;
	allocate_pixel_array();
	set_pixel_array();
}

void picture::make_from_pixels()
{
	bitmap_image output(width, height);

	output.clear();

	int r = 0x0, g = 0x0, b = 0x0;

	for (int i = 0; i < height * width; ++i)
	{
		int x = i % width;
		int y = i / width;

		r = array[i] & 0xff0000;
		r = r >> 0x10;

		g = array[i] & 0xff00;
		g = g >> 0x8;
		
		b = array[i] & 0xff;

		output.set_pixel(x, y, r, g, b);
	}

	output.save_image("output\\" + image_info.output_filename + ".bmp");
}

int picture::run_on_multiple_threads(unsigned number_of_threads, bool assembly, bool benchmark)
{
	std::vector<std::thread> thread_pool;

	unsigned number_of_lines_for_thread = floor(this->width * this->height / number_of_threads/8);
	number_of_lines_for_thread *= 8;
	unsigned leftover = this->width * this->height - number_of_lines_for_thread * number_of_threads;
	unsigned starting_point;
	auto begin = std::chrono::high_resolution_clock::now();

	for(int i = 0; i < number_of_threads; ++i)
	{
		starting_point = number_of_lines_for_thread;
		starting_point *= i;

		if(!assembly) // RUN CPP ALGO
		{
			loader_->load_cpp_lib();
			try
			{				
				thread_pool.push_back(std::thread(&dll_loader::run_cpp, loader_, this->array, this->t_array, number_of_lines_for_thread, image_info.threshold, leftover, starting_point, width, height, number_of_threads));				
			}
			catch (const std::system_error& e)
			{
				std::cerr << "SYSTEM ERROR WITH CODE " << e.code() << "meaning: " << e.what() << "\n";
			}
		} else // RUN ASSEMBLY ALGO
		{
			loader_->load_assembly_lib();

			try
			{
				thread_pool.push_back(std::thread(&dll_loader::run_asm, loader_, this->array, this->t_array, number_of_lines_for_thread, image_info.threshold, leftover, starting_point, width, height, number_of_threads));
			}
			catch (const std::system_error& e)
			{
				std::cerr << "SYSTEM ERROR WITH CODE " << e.code() << "meaning: " << e.what() << "\n";
			}
		}		
	}

	for (auto& t : thread_pool)
	{
		if (t.joinable()) {
			t.join();
		}
	}


	auto end = std::chrono::high_resolution_clock::now();
	auto dur = end - begin;
	auto ms = std::chrono::duration_cast<std::chrono::microseconds>(dur).count();

	if(!benchmark)
		make_from_pixels();

	std::cout << "IT TOOK: " << ms << " uS\n";
	return ms;
}

picture::~picture()
{
	delete[] array;
	delete[] t_array;
	loader_ = nullptr;
	delete loader_;
}

void picture::set_pixel_array()
{
	int r = 0x0, g = 0x0, b = 0x0;

	for(int i = 0; i < height * width; ++i)
	{
		int x = i % width;
		int y = i / width;

		rgb_t colour = image.get_pixel(x, y);
		r = static_cast<int>(colour.red);
		g = static_cast<int>(colour.green);
		b = static_cast<int>(colour.blue);

		int pixel = r << 0x10;
		pixel = pixel | g << 0x8;
		pixel = pixel | b;

		//int pixel = r << 0x18;
		//pixel = pixel | g << 0x10;
		//pixel = pixel | b << 0x8;
		
		array[i] = pixel;

		t_array[i] = 1500000 * sin(M_PI * image_info.alpha*x / 180) * sin(M_PI * image_info.beta*y / 180);
	}

}


void picture::allocate_pixel_array()
{
	int modleft = height * width % 16;
	if(modleft != 0)
	{
		array = new int[height*width + modleft];
		t_array = new int[height*width + modleft];
	} else
	{
		array = new int[height*width];
		t_array = new int[height*width];
	}
}
