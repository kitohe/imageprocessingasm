#pragma once
#include <string>
#include <thread>
#include "utils.h"

class os_communication
{
	utils tool;

public:
	os_communication();
	~os_communication();

	/**
 * \brief This function returns path of file that user has selected
 * \return
 */
	std::string get_file_path();

	unsigned detect_core_count();

	void print_help();

	void check_dirs();
};