#include "menu.h"

void menu::main_menu()
{
	std::cout << "Image halftone approximation program\n";
	system("pause");
	select_version();
}

void menu::select_version()
{
	int mode_choice = 0;

	while (true)
	{
		system("cls");

		std::cout << "Program\n\n";

		std::cout << "SELECT MODE\n\n";
		std::cout << "[1] ASM\n";
		std::cout << "[2] CPP\n";
		std::cout << "[3] RUN BOTH ON THE SAME IMAGE\n";
		std::cout << "[0] EXIT\n";

		std::cin >> mode_choice;

		if (!std::cin)
		{
			std::cin.clear();
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
			continue;
		}

		switch (mode_choice)
		{
		case 0:
			exit(0);
		case 1:
			asm_mode();
			break;
		case 2:
			cpp_mode();
			break;
		case 3:
			both_mode();
		default:
			break;
		}
	}
}

algo_info menu::get_data()
{
	algo_info info;

	std::cout << "Please provide image file:\n";
	info.file_path = comm.get_file_path();

	std::cout << "Please provide thread count: ";
	std::cin >> info.thread_count;

	std::cout << "Please provide threshold (3 bytes between 0x0 - 0xFFFFFF): ";
	std::cin >> std::hex >> info.threshold;
	if (info.threshold > 0xffffff)
		info.threshold = 0xAAAAAA;
	//info.threshold = info.threshold << 0x8;

	std::cout << "Please provide value for alpha: ";
	std::cin >> info.alpha;

	std::cout << "Please provide value for beta: ";
	std::cin >> info.beta;

	std::cout << "Output file name: ";
	std::cin >> info.output_filename;

	return info;
}

void menu::asm_mode()
{
	unsigned core_count = comm.detect_core_count();
	std::cout << "Detected " << core_count << " cores\n";

	auto info = get_data();

	picture* image = image_loader::open_image(info);

	// STACK MULTIPLE RUNS
	std::cout << "Running on: " << info.thread_count << " threads\n";
	image->run_on_multiple_threads(info.thread_count, true);

	image->~picture();

	std::cout << "DONE!";
	system("PAUSE");
}

void menu::cpp_mode()
{
	unsigned core_count = comm.detect_core_count();
	std::cout << "Detected " << core_count << " cores\n";

	auto info = get_data();
	image_loader il;

	picture* image = il.open_image(info);

	// STACK MULTIPLE RUNS
	std::cout << "Running on: " << info.thread_count << " threads\n";
	image->run_on_multiple_threads(info.thread_count);

	image->~picture();

	std::cout << "DONE!";
	system("PAUSE");
}

void menu::both_mode()
{
	unsigned core_count = comm.detect_core_count();
	std::cout << "Detected " << core_count << " cores\n";

	auto info = get_data();
	image_loader il;

	std::string temp_output = info.output_filename;

	info.output_filename += "ASM";
	picture* image = il.open_image(info);

	

	std::cout << "\nRunning ASM version!\n";	
	std::cout << "Running on: " << info.thread_count << " threads\n";
	image->run_on_multiple_threads(info.thread_count, true);

	image->~picture();

	info.output_filename.clear();
	info.output_filename = temp_output + "CPP";
	image = il.open_image(info);

	std::cout << "\nRunning CPP version!\n";	
	std::cout << "Running on: " << info.thread_count << " threads\n";
	image->run_on_multiple_threads(info.thread_count, false);

	image->~picture();

	std::cout << "DONE!";
	system("PAUSE");

}

