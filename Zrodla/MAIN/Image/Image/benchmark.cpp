#include "benchmark.h"

void benchmark::clean_up()
{
	for (auto&it : pictures) {
		it->~picture();
	}
}

void benchmark::setup()
{

	for (int i = 0; i < this->image_count*2; ++i)
	{
		image_info.file_path = "benchmark\\b" + std::to_string(i/2 + 1) + ".bmp";
		//image_info.output_filename = "b" + std::to_string(i + 1);
		pictures.push_back(image_loader::open_image(image_info));
	}
}

void benchmark::run()
{
	setup();
	output_file.open("results.txt", std::ios::app);
	output_file << "THREADS: " << image_info.thread_count << "\n"
		<< "PASSES SET: " << this->passes_count
		<< "\nRESULTS IN uS\n";
	for(volatile int i = 0; i < this->image_count; ++i)
	{	
		std::cout << "PASS: " << i << "\n";
		output_file << "IMAGE: " << i + 1 << "\nASM TIMES:\n" ;
		for(volatile int j = 0; j < this->passes_count; ++j)
		{				
			output_file <<
			pictures.at(i)->run_on_multiple_threads(image_info.thread_count, false, true) << "\n";
		}
		output_file << "CPP TIMES:\n";
		for (volatile int j = 0; j < this->passes_count; ++j)
		{
			output_file <<
				pictures.at(i + 1)->run_on_multiple_threads(image_info.thread_count, true, true) << "\n";
		}
	}
	
	std::cout << "BENCHMARK FINISHED SUCCESSFULLY!\n";
	output_file.close();
	clean_up();
}
