#pragma once

#include <algorithm>
#include <vector>
#include <string>
#include <sstream>
#define NOMINMAX
#include <windows.h>
#include <iostream>

class utils
{
public:
	/**
	 * \brief This function converts lpwstr to string
	 * \param in
	 * \param codepage
	 * \return
	 */
	std::string lpwstr_to_string(const LPWSTR in, UINT codepage = CP_ACP);
	std::wstring s2ws(const std::string& s);
};
