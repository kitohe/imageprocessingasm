#pragma once
#include "dll_loader.h"
#include "image_loader.h"

#include <vector>

class benchmark
{
	unsigned passes_count;
	unsigned image_count;

	std::ofstream output_file;

	algo_info image_info;
	std::vector<picture*> pictures;
	std::vector<std::string> names;
	std::vector<algo_info> info;
	void clean_up();
	void setup();

public:
	benchmark(unsigned passes, unsigned images, unsigned alpha, unsigned beta, std::string threshold, unsigned threads) :
		passes_count(passes), image_count(images)
	{
		image_info.threshold = std::stoi(threshold, nullptr, 16);
		image_info.alpha = alpha;
		image_info.beta = beta;
		image_info.thread_count = threads;
	}

	void run();
};