#include "utils.h"

std::string utils::lpwstr_to_string(const LPWSTR in, UINT codepage)
{
	char * temp = 0;
	std::string out;

	int bytes_required = WideCharToMultiByte(codepage, 0, in, -1, 0, 0, 0, 0);

	if (bytes_required > 0)
	{
		temp = new char[bytes_required];
		int rc = WideCharToMultiByte(codepage, 0, in, -1, temp, bytes_required, 0, 0);

		if (rc != 0)
		{
			temp[bytes_required - 1] = 0;
			out = temp;
		}
	}
	delete[] temp;
	return out;
}

std::wstring utils::s2ws(const std::string& s)
{
	int len;
	int slength = (int)s.length() + 1;
	len = MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, 0, 0);
	wchar_t* buf = new wchar_t[len];
	MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, buf, len);
	std::wstring r(buf);
	delete[] buf;
	return r;

}
