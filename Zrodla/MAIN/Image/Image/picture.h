#pragma once
#include "bitmap_image.hpp"
#include "dll_loader.h"
#include "algo_type.h"
#include <thread>
#include <vector>
#include <chrono>
#include <cmath>

#define M_PI 3.14159265358979323846

class picture
{
	algo_info image_info;

	bitmap_image image;

	dll_loader* loader_;

	void allocate_pixel_array();

	void set_pixel_array();

public:
	int width;

	int height;

	int** pixel_array;

	int** transition_array;

	int* array;

	int* t_array;

	picture(bitmap_image image, algo_info info);

	void make_from_pixels();

	int run_on_multiple_threads(unsigned number_of_threads, bool assembly = false, bool benchmark = false);

	void calculate_new_pixels(int** pixel_array, unsigned number_of_lines,
		unsigned leftover, unsigned starting_point);

	~picture();
};
