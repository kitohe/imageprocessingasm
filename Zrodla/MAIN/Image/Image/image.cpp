// Image.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <stdio.h>
#include <iostream>
#include <Windows.h>

#include "menu.h"
#include "cxxopts.hpp"
#include "benchmark.h"

typedef int(__cdecl *ASMPROC)(int, int);
typedef int(__stdcall *CPROC)(int, int);

int main(int argc, char* argv[])
{
	try
	{
		cxxopts::Options options("ImageProcessing", "Program that does half-tone image approximation");
		os_communication os_communication;
		algo_info image_info;

		int alpha = 0;
		int beta = 0;
		int threads = os_communication.detect_core_count();
		int threshold_numeric = 0;
		int runs = 1;
		bool run_asm = false;
		std::string threshold;
		std::string path;
		std::string output;

		options.add_options()
			("i,input", "Input image path (.bmp only)", cxxopts::value<std::string>(path))
			("o,output", "Output image name", cxxopts::value<std::string>(output))
			("a,alpha", "Alpha multiplier", cxxopts::value<int>(alpha))
			("b,beta", "Beta multiplier", cxxopts::value<int>(beta))
			("t,threshold", "Pixel threshold", cxxopts::value<std::string>(threshold))
			("n,threads", "Number of threads to run", cxxopts::value<int>(threads))
			("r,run", "Run amount", cxxopts::value<int>(runs))
			("h,help", "Help")
			("asm", "Run asm")
			("both", "Run both")
			("benchmark", "Benchmark mode");

		os_communication.check_dirs();

		auto result = options.parse(argc, argv);

		if (result.count("h"))
		{
			os_communication.print_help();
			exit(0);
		}

		if (result.count("benchmark"))
		{
			benchmark bench(5, 5, alpha, beta, threshold, 1);
			bench.run();

			for (int i = 2; i <= threads; i*=2) {
				benchmark bench(5, 5, alpha, beta, threshold, i);
				bench.run();
				
			}
			exit(0);
		}

		if (alpha == 0 || beta == 0 || path.empty() || output.empty())
		{
			std::cout << "Not all necessary values were specified, running menu for you...\n";
			menu menu;
			menu.main_menu();
		}		

		if(result.count("i"))
		{
			std::cout << "Looking for image at specified path: " << path << "\n";
			image_info.file_path = path;
		}
		if(result.count("o"))
		{
			std::cout << "File will be saved under name: " << output << "\n";
			image_info.output_filename = output;
		}
		if(result.count("a"))
		{
			std::cout << "Alpha set to: " << alpha << "\n";
			image_info.alpha = alpha;
		}
		if (result.count("b"))
		{
			std::cout << "Beta set to: " << beta << "\n";
			image_info.beta = beta;
		}
		if(result.count("t"))
		{
			std::cout << "Threshold set to: " << threshold << "\n";
			threshold_numeric = std::stoi(threshold,nullptr,16);			
			image_info.threshold = threshold_numeric;
		}
		if (result.count("n"))
		{
			std::cout << "Number of threads to run set to: " << threads << "\n";
			image_info.thread_count = threads;
		}
		if(result.count("asm"))
		{
			std::cout << "Running asm version!\n";
			run_asm = true;
		}
		if(result.count("both"))
		{
			std::cout << "Running both versions!\n\n";
				

			for(int i = 0; i < runs; ++i)
			{
				std::cout << "\nRunning ASM version!\n";
				image_info.output_filename += "ASM";
				picture* image_asm = image_loader::open_image(image_info);
				image_asm->run_on_multiple_threads(image_info.thread_count, true);

				image_asm->~picture();

				std::cout << "Running CPP version!\n";
				image_info.output_filename.clear();
				image_info.output_filename = output + "CPP";
				picture* image_cpp = image_loader::open_image(image_info);
				image_cpp->run_on_multiple_threads(image_info.thread_count);

				image_cpp->~picture();
			}
			system("pause");
			return 0;
		}

		picture* image = image_loader::open_image(image_info);
		image->run_on_multiple_threads(image_info.thread_count, run_asm);

		image->~picture();
	}
	catch (const cxxopts::OptionException& e)
	{
		std::cout << "error parsing options: " << e.what() << std::endl;
		exit(-1);
	}

	return 0;
}
