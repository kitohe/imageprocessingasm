#include "image_loader.h"
#include <iostream>

picture* image_loader::open_image(algo_info info)
{
	bitmap_image image(info.file_path);

	if(!image)
	{
		std::cerr << "Error - Failed to open image";
	}

	picture* pic = new picture(image, info);
	
	return pic;
}
