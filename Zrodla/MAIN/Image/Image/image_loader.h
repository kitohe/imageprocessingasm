#pragma once

#include "bitmap_image.hpp"
#include "picture.h"

#include <string>

class image_loader
{
public:
	static picture* open_image(algo_info info);
};
