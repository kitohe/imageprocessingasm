#include "dll_loader.h"

bool dll_loader::load_assembly_lib()
{
	hinstance_ = LoadLibraryA(asm_lib_path);

	if (hinstance_ != nullptr)
	{
		asm_fun = (assembly_function)GetProcAddress(hinstance_, "process_img");
		if (asm_fun != NULL)
			return true;
	}
	return false;
}

bool dll_loader::load_cpp_lib()
{
	hinstance_ = LoadLibraryA(cpp_lib_path);

	if (hinstance_ != nullptr)
	{
		cpp_fun = (cpp_function)GetProcAddress(hinstance_, "process_img");
		if(cpp_fun != NULL) 
			return true;
	}
	return false;
}

void dll_loader::run_asm(int* array, int* t_array, unsigned number_of_lines, int threshold,
	unsigned leftover, unsigned starting_point, unsigned width, unsigned height, unsigned number_of_threads)
{
	asm_fun(array, t_array, number_of_lines, threshold, leftover, starting_point, width, height, number_of_threads);
}

void dll_loader::run_cpp(int* array, int* t_array, unsigned number_of_lines, int threshold,
	unsigned leftover, unsigned starting_point, unsigned width, unsigned height, unsigned number_of_threads)
{
	cpp_fun(array, t_array, number_of_lines, threshold, leftover, starting_point, width, height, number_of_threads);
}
