#pragma once
#include <string>
#include <Windows.h>
class dll_loader
{
	typedef int(__cdecl *assembly_function)(int* array, int* t_array, unsigned number_of_lines, int threshold,
		unsigned leftover, unsigned starting_point, unsigned width, unsigned height, unsigned number_of_threads);
	typedef int(__stdcall *cpp_function)(int* array, int* t_array, unsigned number_of_lines, int threshold,
		unsigned leftover, unsigned starting_point, unsigned width, unsigned height, unsigned number_of_threads);

	//LPCSTR asm_lib_path = "C:\\Users\\dmadr\\Documents\\Projects\\Cpp\\Image\\Zrodla\\MAIN\\Image\\x64\\Debug\\DLL_ASM.dll";
	//LPCSTR cpp_lib_path = "C:\\Users\\dmadr\\Documents\\Projects\\Cpp\\Image\\Zrodla\\MAIN\\Image\\x64\\Debug\\DLL_C.dll";

	LPCSTR cpp_lib_path = "DLL_C.dll";
	LPCSTR asm_lib_path = "DLL_ASM.dll";

	HINSTANCE hinstance_;

	BOOL free_result, fRunTimeLinkSuccess = FALSE;

public:
	assembly_function asm_fun;
	cpp_function cpp_fun;

	bool load_assembly_lib();

	bool load_cpp_lib();

	void run_asm(int* array, int* t_array, unsigned number_of_lines, int threshold,
		unsigned leftover, unsigned starting_point, unsigned width, unsigned height, unsigned number_of_threads);

	void run_cpp(int* array, int* t_array, unsigned number_of_lines, int threshold,
		unsigned leftover, unsigned starting_point, unsigned width, unsigned height, unsigned number_of_threads);
};
