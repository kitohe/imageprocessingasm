#include "os_communication.h"
#include "common_file_dialog.hpp"
#include "utils.h"

os_communication::os_communication()
= default;

os_communication::~os_communication()
= default;

std::string os_communication::get_file_path()
{
	HRESULT hr = CoInitializeEx(NULL, COINIT_APARTMENTTHREADED | COINIT_DISABLE_OLE1DDE);
	LPWSTR z = BasicFileOpen();

	// String to path which user has specified
	std::string path = tool.lpwstr_to_string(z);

	return path;
}

unsigned os_communication::detect_core_count()
{
	unsigned threads = std::thread::hardware_concurrency();
	return threads;
}

void os_communication::print_help()
{
	std::cout << "USAGE: image [OPTION]\n"
		<< "Perform half-tone approximation on image\n\n"
		<< "Mandatory arguments:\n"
		<< "-i			input file must be .bmp extension\n"
		<< "-o			output file name will be saved at 'output' folder\n"
		<< "-a			alpha multiplier has effect on calculations\n"
		<< "-b			beta multiplier has effect on calculations\n"
		<< "-m			multiplier has effect on calculations\n"
		<< "-n			number of threads to run (default - count of logical CPU threads\n"
		<< "-h			prints help\n";
}

void os_communication::check_dirs()
{
	std::wstring stemp = tool.s2ws("output");
	std::wstring stempp = tool.s2ws("benchmark");
	if (!CreateDirectory(stemp.c_str(), NULL))
	{
		if(GetLastError() == ERROR_ALREADY_EXISTS)
		{
			return;
		} else
		{
			
		}

	}

	if (!CreateDirectory(stempp.c_str(), NULL))
	{
		if (GetLastError() == ERROR_ALREADY_EXISTS){}
		else{}
	}
}
