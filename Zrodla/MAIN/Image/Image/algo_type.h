#pragma once
#include <string>

struct algo_info
{
	std::string file_path;
	std::string output_filename;
	unsigned alpha;
	unsigned beta;
	unsigned thread_count;
	int threshold;
};
